package com.example.ServletAuth.service;

import com.example.ServletAuth.connection.MysqlConnectionConfig;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Validate {
    public static boolean checkUser(String email,String password)
    {
        boolean st =false;
        try {

            //loading drivers for mysql

            //creating connection with the database
            Connection connection = MysqlConnectionConfig.getConnectionToMysql();

            //Connection con = DriverManager.getConnection("jdbc:mysql://localhost/test","root","password");
            PreparedStatement ps = connection.prepareStatement("select * from servletpractice where email=? and password=?");
            ps.setString(1, email);
            ps.setString(2, password);
            ResultSet rs =ps.executeQuery();
            st = rs.next();

        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return st;
    }
}
