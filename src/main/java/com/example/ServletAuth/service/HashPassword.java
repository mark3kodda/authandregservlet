package com.example.ServletAuth.service;

import java.util.Random;

public class HashPassword {

    //как сюда попадает соль *(вроде она лежит в проперти файле)

    public String saltingPassword(String salt, String password) {

        String strSaltAndPass = salt + password;

        int hash = 7;
        for (int i = 0; i < strSaltAndPass.length(); i++) {
            hash = hash * 31 + strSaltAndPass.charAt(i);
        }

        String saltedPassword = String.valueOf(hash);

        return saltedPassword;
    }
}
