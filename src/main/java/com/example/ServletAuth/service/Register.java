package com.example.ServletAuth.service;

import com.example.ServletAuth.model.User;
import com.google.gson.Gson;


import java.io.*;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.*;
import java.util.Properties;


public class Register extends HttpServlet {

    Gson gson = new Gson();
    Dto dto = new Dto(gson);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");
            BufferedReader textBody = request.getReader();
            dto.preparingUserDataToDB(textBody.toString());
    }

    private void sendEmail(String email){
        final String fromEmail = "marselium@gmail.com"; //requires valid gmail id
        final String password = "eniogegnzbcqotdb\n"; // correct password for gmail id
        final String toEmail = email; // can be any email id

        System.out.println("SSLEmail Start");
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
        props.put("mail.smtp.socketFactory.port", "465"); //SSL Port
        props.put("mail.smtp.socketFactory.class",  "javax.net.ssl.SSLSocketFactory"); //SSL Factory Class
        props.put("mail.smtp.auth", "true"); //Enabling SMTP Authentication
        props.put("mail.smtp.port", "465"); //SMTP Port

        Authenticator auth = new Authenticator() {
            //override the getPasswordAuthentication method
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromEmail, password);
            }
        };

        Session session = Session.getDefaultInstance(props, auth);
        System.out.println("Session created");
        sendEmailExtra(session, toEmail,"Code for resetting password", "Your code: 12345");

    }
    public static void sendEmailExtra(Session session, String toEmail, String subject, String body){
        try
        {
            MimeMessage msg = new MimeMessage(session);
            //set message headers
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress("marselium@gmail.com"));
            msg.setReplyTo(InternetAddress.parse(toEmail));
            msg.setSubject(subject, "UTF-8");
            msg.setText(body, "UTF-8");

//            msg.setSentDate(new Date());
            msg.setSentDate(new java.util.Date());

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
            System.out.println("Message is ready");
            Transport.send(msg);

            System.out.println("EMail Sent Successfully!!");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


}