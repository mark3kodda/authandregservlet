package com.example.ServletAuth.service;

import com.example.ServletAuth.model.User;
import com.google.gson.Gson;

public class Dto {
    Gson gson;
    HashPassword hashPassword = new HashPassword();

    public Dto(Gson gson) {
        this.gson = gson;
    }

    public boolean preparingUserDataToDB(String textBody){

        User user = gson.fromJson(textBody,User.class);
        user.setPassword(hashPassword.saltingPassword("someSecretSalt",user.getPassword()));

        /*
        //валидация Алёна
        if(validation == true){
            createRecordToDB();
            return true;
        }else{
            return false;
        }

         */
        return true;

    }

}
