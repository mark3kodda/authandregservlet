package com.example.ServletAuth.connection;

public class FailedConnectionException extends Exception {

    public FailedConnectionException(String message) {
        super(message);
    }
}
