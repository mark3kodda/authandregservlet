package com.example.ServletAuth.connection;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlConnectionConfig {

    //private static Logger logger = LogManager.getLogger(MysqlConnectionConfig.class);
    private static final String DB_URL = "jdbc:mysql://localhost/test";
    private static final String USER = "root";
    private static final String PASSWORD = "rootpassword";

    public static Connection getConnectionToMysql() throws FailedConnectionException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("Connected successfully to DB");
            return DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println("Failed to connect with DB");
            ex.printStackTrace();
//            logger.info("MySql FAIL to connect");
//            logger.error(ex.getMessage());

            throw new FailedConnectionException(ex.getMessage());
        }
    }
}


